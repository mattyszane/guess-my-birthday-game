from random import randint

name = input("Hi! what is your name: ")
guess_num = 0
for guess_num in range(1, 6):
    month = randint(1, 12)
    year = randint (1924, 2004)

    print("Guess", guess_num, name, "were you born in", month, "/", year, "?" )
    response = input("yes or no: ")

    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_num == 5:
        print("I have other things to do, goodbye! See you next time!")
    else:
        print("Drat, lemme try again!")
